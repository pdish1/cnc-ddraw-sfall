
## cnc-ddraw-sfall
*a simple variant to make cnc-ddraw compatible with Fallout's sfall and High Resolution Patch (HRP)*  
https://gitlab.com/pdish1/cnc-ddraw-sfall/-/releases  

## Features
 - compatible with crafty sfall1 for Fallout 1 and its patches (Fixt/TeamX)
 - compatible with crafty sfall2; sfall 3.x; sfall 4.x; sfall 5.x; for Fallout 2 and its TCs (Resurrection/Nevada/EtTu...)
 - extended black backgrounds to all 4:3 screens if *`DIALOG_SCRN_BACKGROUND=1`* in `f1_res.ini`/`f2_res.ini` (character screen, save/load screen, pipboy...)
 - built-in fps limiter
 - easy upscaling/windowing/alt-tabbing
 - only to be used with old Fallout, for other games use the original https://github.com/FunkyFr3sh/cnc-ddraw/  
 - ...


## Instructions
 1. In `f1_res.ini`/`f2_res.ini` set
	```ini
	[MAIN]
	UAC_AWARE=0
	```
2. Extract cnc-ddraw into the game directory.   

> Do not scale with HRP/sfall, set the viewport size and let cnc-ddraw upscale the image.  
> Do not set windowed with HRP. Use cnc-ddraw to do the windowing.   


## Additional Context
*`DIALOG_SCRN_BACKGROUND`* patch works only under HRP 4.1.8 (this is the version that's distributed with GOG/Steam/...)  

    f1_res.dll 4.1.8 CRC32: 0x9e8e0345
    f2_res.dll 4.1.8 CRC32: 0xe0795583

## Compatibility

crafty sfall1, sfall2 -> http://fforum.kochegarov.com/index.php?showtopic=29288  
sfall 4 -> https://github.com/sfall-team/sfall (last version tested `4.4.3.1`)  
sfall 5 -> https://gitflic.ru/project/fakelshub/sfall/ (last version tested `5.0.7.4`)  


|                  	|    **w/o sfall**   	|  **crafty sfall**  	|     **sfall 4**    	|     **sfall 5**    	|
|:----------------:	|:------------------:	|:------------------:	|:------------------:	|:------------------:	|
|    **w/o HRP**   	| :heavy_check_mark: 	| :heavy_check_mark: 	| :heavy_check_mark: 	| :heavy_check_mark: 	|
| **built-in HRP** 	|         N/A        	|         N/A        	| :heavy_check_mark: 	| :heavy_check_mark: 	|
|   **Mash HRP**   	| :heavy_check_mark: 	| :heavy_check_mark: 	| :heavy_check_mark: 	| :heavy_check_mark: 	|
